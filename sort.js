const swap = (left, right, array) => {
  const tmp = array[left];
  array[left] = array[right];
  array[right] = tmp;
};

const getPivot = (a, b, c) => {
  if (a > b) {
    if (b > c) {
      return b;
    } else {
      return a > c ? c : a;
    }
  } else {
    if (a > c) {
      return a;
    } else {
      return b > c ? c : b;
    }
  }
};

const bubblesort = (array) => {
  for (let i = 0; i < array.length - 1; i++) {
    for (let j = array.length - 1; i < j; j--) {
      let swapped = false; // 描画用
      const left = j - 1;
      const right = j;
      if (array[left] > array[right]) {
        swap(left, right, array);
        swapped = true; // 描画用
      }
      record(memoryB, right, swapped, array); // 描画用
    }
  }
};

const quicksort = (array, left = 0, right = -1) => {
  if (right === -1) {
    right = array.length - 1;
  }
  if (right <= left) {
    return;
  }
  let l = left;
  let r = right;
  const p = getPivot(array[l], array[l + 1], array[r]);
  while (true) {
    while (array[l] < p) {
      record(memoryQ, l, false, array); // 描画用
      l++;
    }
    while (p < array[r]) {
      record(memoryQ, r, false, array); // 描画用
      r--;
    }
    if (r <= l) {
      break;
    }
    swap(l, r, array);
    record(memoryQ, r, true, array); // 描画用
    l++;
    r--;
  }
  quicksort(array, left, l - 1);
  quicksort(array, r + 1, right);
};
