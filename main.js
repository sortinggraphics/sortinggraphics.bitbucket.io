const memoryB = {
  frame: 0,
  map: [],
  swapCount: 0,
};
const memoryQ = {
  frame: 0,
  map: [],
  swapCount: 0,
};

const shuffle = (array) => {
  for (let i = array.length - 1; 0 <= i; i--) {
    const rand = Math.floor(Math.random() * (i + 1));
    [array[i], array[rand]] = [array[rand], array[i]];
  }
};

const record = (memory, right, swapped, array) => {
  memory.map.push(
      {
        selected: right, // 比較している右側
        swapped: swapped, // 交換したターンならtrueになる
        array: array.concat(), // コピーして渡す
      });
};

// バーを表示
const drawBar = (p, array, selected) => {
  const under = 350;
  const gridPixel = 10;
  const marginLeft = 4;
  for (let i = 0; i < array.length; i++) {
    // gray
    const left = (gridPixel + marginLeft) * i + 2;
    const height = gridPixel * array[i];
    // 枠線の色
    p.stroke(0, 0, 0);
    if (i === selected) {
      // 基準値の色バーの色
      p.fill(p.color(191, 164, 65));
    } else {
      // バーの色
      p.fill(p.color(237, 235, 213));
    }
    // rect(left, top, width, height);
    p.rect(left, under - height, gridPixel, height);
    p.fill(p.color(50, 50, 50));
    p.rect(left, under - height, gridPixel, gridPixel);
  }
};

const getCloneArray = (origin) => {
  const clone = [];
  for (let i = 0; i < origin.length; i++) {
    clone[i] = origin[i];
  }
  return clone;
};

const initMemory = (memory) => {
  memory.frame = 0;
  memory.map = [];
  memory.swapCount = 0;
};

const start = () => { // eslint-disable-line no-unused-vars
  initMemory(memoryB);
  initMemory(memoryQ);

  const length = 30;
  document.getElementById('lengthB').innerHTML = String(length);
  document.getElementById('lengthQ').innerHTML = String(length);

  const arrayB = [];
  for (let i = 0; i < length; i++) {
    arrayB.push(i + 1);
  }
  shuffle(arrayB);
  const arrayQ = getCloneArray(arrayB);

  bubblesort(arrayB);
  quicksort(arrayQ);
};

const createSketch = (canvasId, counterId, memory) => {
  const sketch = (p) => {
    p.setup = () => {
      const canvas = p.createCanvas(420, 400);
      canvas.parent(canvasId);
      p.frameRate(100);
      p.background(p.color(212, 236, 234));
    };
    p.draw = () => {
      if (0 <= memory.frame && memory.frame < memory.map.length) {
        p.background(p.color(212, 236, 234));
        const m = memory.map[memory.frame];
        drawBar(p, m.array, m.selected);
        if (m.swapped) {
          /* 交換したターンである */
          memory.swapCount++;
          document.getElementById(counterId).innerHTML = memory.swapCount;
        }
        memory.frame++;
      }
    };
  };
  return sketch;
};

const createP5 = (canvasId, counterId, memory) => {
  const sketch = createSketch(canvasId, counterId, memory);
  new p5(sketch, canvasId);
};

const main = () => {
  createP5('canvasB', 'countB', memoryB);
  createP5('canvasQ', 'countQ', memoryQ);
};

main();
